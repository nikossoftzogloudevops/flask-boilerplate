# Flask boilerplate example repo

Example of the following things;

* Flask app with Blueprints.
* Building a container image using Buildah in a Gitlab CI pipeline.
* Deployment to k8s using kustomize.
* Deploying different environments from private branches using kustomization overlays.
* Shared state by using redis in k8s application.

# Deployment how to

## CI/CD Variables

Create the following CI/CD variables in Gitlab Settings.

* ``REGISTRY_AUTH_JSON`` - A File with JSON auth credentials for your container registry, [see example here](https://github.com/GoogleContainerTools/kaniko/blob/master/README.md#pushing-to-docker-hub).
* ``REGISTRY_IMAGE`` - A Variable with URI for your registry image, in my case it's ``registry.hub.docker.com/stemid/flask-boilerplate``.
* ``REGISTRY_TAG`` - This variable is set automatically based on Gitlab Environment variables like branch/tag name and commit SHA.
* ``KUBECONFIG`` - A File with your CI users kubeconfig.
* ``K8S_NAMESPACE`` - Namespace for the deployment.
* ``OVERRIDE_OVERLAY`` - Optional variable to override the overlay used by kustomize.

## Kustomization overlays

Inside the ``kustomize/overlays`` directory you must create your own overlay with the name matching the value of ``$CI_COMMIT_REF_NAME``, [see example here](https://docs.gitlab.com/ee/ci/variables/#list-all-environment-variables).

In other words it's the branch or tag name, depending on if you're pushing to a branch or a tag. So in my case I've created master as the default overlay. Someone else will create their own branch, with their own overlay, push to their branch and it will deploy that branch's overlay.

## Private container image registry

The deployment job places the file ``REGISTRY_AUTH_JSON`` under ``overlays/<your overlay>/secrets/.dockerconfigjson`` in case you want to use it as imagePullSecrets in a private branch/overlay.

# App development

Just some words about the Python app, not directly relating to k8s deployment.

## Install

    $ pip install -r requirements.txt

## Run

    $ FLASK_ENV=dev python run.py

## Test

    $ curl -s 'http://localhost:5000/api/v1/client/ip'

## Environment

Copy ``.env`` to your own ``.env.myenv`` and set the environment variable ``FLASK_ENV=myenv`` to use that file.

Then for example you can build a docker image like this.

    $ docker build --build-arg FLASK_ENV=myenv -t user/flask_boilerplate:myenv

### Environment example

```
APP_LISTEN_HOST=127.0.0.1
APP_DEBUG=yes
# Comment this line out to not use redis
STORE_STATE=yes
REDIS_HOST=localhost
REDIS_PORT=6379
```
